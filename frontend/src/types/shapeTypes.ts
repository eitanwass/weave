import Konva from "konva";
import ShapeConfig = Konva.ShapeConfig;

export type generalShape = {
	type: string
} & ShapeConfig;

export type position = {
	x: number,
	y: number
}
import React from 'react';
import {generalShape, position} from "./shapeTypes";

export type setCurrentShapeType = React.Dispatch<React.SetStateAction<generalShape | undefined>>;

export type addMapShapeType = (shape: generalShape) => void;

export type editorModeType = {
	onMouseDown?: (evt: MouseEvent, setCurrentShape: setCurrentShapeType, addShape: addMapShapeType) => void,
	onMouseMove?: (evt: MouseEvent, setCurrentShape: setCurrentShapeType, addShape: addMapShapeType) => void,
	onMouseUp?: (evt: MouseEvent, setCurrentShape: setCurrentShapeType, addShape: addMapShapeType) => void,

	onKeyDown?: (evt: React.KeyboardEvent<HTMLDivElement>, setCurrentShape: setCurrentShapeType) => void,
};

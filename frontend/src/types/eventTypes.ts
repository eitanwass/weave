import React from "react";

export type inputEventsType = {
	onMouseDown: (evt: MouseEvent) => void,
	onMouseMove: (evt: MouseEvent) => void,
	onMouseUp: (evt: MouseEvent) => void,
};

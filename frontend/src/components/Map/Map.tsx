import _ from "lodash";
import {Layer, Stage, Rect, Label, Text} from "react-konva";
import React, {ReactNode, useCallback, useEffect, useMemo, useState} from "react";
import {createSurroundingGrid} from "../BackgroundGrid/BackgroundGrid";
import {useGlobalMousePosition} from "../../contexts/GlobalMousePositionContext";
import {inputEventsType} from "../../types/eventTypes";
import {generalShape} from "../../types/shapeTypes";
import Konva from "konva";
import RectConfig = Konva.RectConfig;

type mapPropsType = {
	mapShapes: generalShape[],
	currentEditShape?: generalShape,
} & inputEventsType

const Map = ({mapShapes, currentEditShape, ...mouseEvents}: mapPropsType) => {
	const [stagePos, setStagePos] = useState({x: 0, y: 0});

	const {globalMousePosition, setGlobalMousePosition} = useGlobalMousePosition();

	const backgroundGrid = useMemo(() => createSurroundingGrid(globalMousePosition), [globalMousePosition])

	const updateGlobalMousePosition = useCallback(({evt: {clientX, clientY}}: {evt: {clientX: number, clientY: number}}) => {
		setGlobalMousePosition({x: clientX - stagePos.x, y: clientY - stagePos.y});
	}, [stagePos]);

	const onMouseMove = (evt: MouseEvent) => {
		updateGlobalMousePosition({evt});
		mouseEvents.onMouseMove(evt);
	};

	const onScroll = ({evt}: { evt: WheelEvent }) => {
		let [deltaX, deltaY] = [evt.deltaX, evt.deltaY];
		if (evt.shiftKey) {
			[deltaX, deltaY] = [deltaY, deltaX]
		}

		setStagePos(({x, y}) => ({x: x - deltaX, y: y - deltaY / 2}));
		onMouseMove(evt);
	}

	const shapeToKonva = (shape: generalShape): ReactNode => (
		<shape.type
			{
				..._.omit(shape, ["type"])
			}
		/>
	);

	const getShapeSizeTooltips = (shape: generalShape): ReactNode => {
		if (shape.type !== "Rect")
			return <></>;

		shape.x ||= 0;
		shape.y ||= 0;
		shape.width ||= 0;
		shape.height ||= 0;

		const widthTooltip = (
			<Label
				x={shape.x + shape.width / 2}
				y={shape.y}
			>
				<Text text={`width: ${shape.width}`} fill={"white"}/>
			</Label>
		);

		const heightTooltip = (
			<Label
				x={shape.x}
				y={shape.y + shape.height / 2}
			>
				<Text text={`height: ${shape.height}`} fill={"white"}/>
			</Label>
		);

		return <>
			{widthTooltip}
			{heightTooltip}
			</>
	}

	return (
		<Stage
			x={stagePos.x}
			y={stagePos.y}
			width={window.innerWidth}
			height={window.innerHeight}

			onMouseDown={({evt}) => mouseEvents.onMouseDown(evt)}
			onMouseMove={({evt}: { evt: MouseEvent }) => onMouseMove(evt)}
			onMouseUp={({evt}) => mouseEvents.onMouseUp(evt)}
			onWheel={onScroll}
		>
			<Layer>
				{ mapShapes.map(shapeToKonva) }
			</Layer>
			<Layer>
				{
					currentEditShape === undefined ? <></> : (
						<>
							{shapeToKonva(currentEditShape)}
							{/*{getShapeSizeTooltips(currentEditShape)}*/}
						</>
					)
				}
			</Layer>
			<Layer>
				{backgroundGrid}
			</Layer>
		</Stage>
	)
}

export default Map;
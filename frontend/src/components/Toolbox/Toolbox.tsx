import React, {ReactNode} from "react";

import "./Toolbox.css";


const Toolbox = ({children}: {children: ReactNode}) => {
	return (
		<div className={"toolbox-container"}>
			{children}
		</div>
	);
};

export default Toolbox;

import React, {forwardRef, useImperativeHandle, useState} from "react";
import {addMapShapeType, editorModeType, setCurrentShapeType} from "../../../types/editorModesTypes";
import {snapToGrid} from "../../BackgroundGrid/BackgroundGrid";
import * as KeyCode from "keycode-js";
import {useGlobalMousePosition} from "../../../contexts/GlobalMousePositionContext";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faSquare} from "@fortawesome/free-solid-svg-icons";
import {position} from "../../../types/shapeTypes";
import {useColor} from "../../../contexts/ColorContext";
import _ from "lodash";


export type FillItemHandle = {
	fillItem: editorModeType
}

// Rename to DrawRect
const FillItem = forwardRef<FillItemHandle>(({}, ref) => {
	const {globalMousePosition} = useGlobalMousePosition();
	const {color} = useColor();

	const [isDrawing, setIsDrawing] = useState(false);

	const [firstVertex, setFirstVertex] = useState<position>({x:0, y:0});

	const applyFinalShape = (setCurrentShape: setCurrentShapeType, addShape: addMapShapeType) => {
		setCurrentShape((curShape) => {
			if (curShape !== undefined)
				addShape(curShape);
			return undefined;
		});
	}

	useImperativeHandle(ref, () => ({
		fillItem: {
			onMouseDown: (evt, setCurrentShape, addShape) => {
				if (!isDrawing) {
					// Starting to draw
					setFirstVertex(snapToGrid(globalMousePosition));
				}
				else {
					// Finish drawing
					applyFinalShape(setCurrentShape, addShape);
				}
				setIsDrawing((cur) => !cur);
			},
			onMouseMove: (evt, setCurrentShape, addShape) => {
				if (!isDrawing)
					return;

				const secondVertex = snapToGrid(globalMousePosition);

				const rectVertices = [
					{x: 0, y: 0},
					{x: 0, y: 0},
				]

				rectVertices[0].x = firstVertex.x + Number(secondVertex.x < firstVertex.x) * 50;
				rectVertices[1].x = secondVertex.x + Number(secondVertex.x >= firstVertex.x) * 50;

				rectVertices[0].y = firstVertex.y + Number(secondVertex.y < firstVertex.y) * 50;
				rectVertices[1].y = secondVertex.y + Number(secondVertex.y >= firstVertex.y) * 50;


				setCurrentShape({
					key: `${rectVertices[0].x},${rectVertices[0].y}-${rectVertices[1].x},${rectVertices[1].y}`,
					type: "Shape",
					fill: color,
					sceneFunc: (context, shape) => {
						context.beginPath();
						context.moveTo(rectVertices[0].x, rectVertices[0].y);
						context.lineTo(rectVertices[0].x, rectVertices[1].y);
						context.lineTo(rectVertices[1].x, rectVertices[1].y);
						context.lineTo(rectVertices[1].x, rectVertices[0].y);
						context.closePath();

						context.fillStrokeShape(shape);
					},
				});
			},
			onMouseUp: (evt, setCurrentShape, addShape) => {
				if (!isDrawing)
					return;

				// Finish drawing using drag. Else, find other point later
				if (!_.isEqual(snapToGrid(globalMousePosition), firstVertex)) {
					applyFinalShape(setCurrentShape, addShape);
					setIsDrawing(false);
				}
			},

			onKeyDown: (evt, setCurrentShape) => {
				if (KeyCode.CODE_ESCAPE === evt.key) {
					setCurrentShape(undefined);
				}
			}
		}
	}));


	return (
		<div>
			<FontAwesomeIcon icon={faSquare} /> Fill
		</div>
	);
});


export default  FillItem;

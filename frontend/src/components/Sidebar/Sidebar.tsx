import React, {ReactNode} from "react";

import "./Sidebar.css";


const Sidebar = ({children}: {children: ReactNode}) => {
	return (
		<div className={"sidebar"}>
			{children}
		</div>
	)
};

export default Sidebar;

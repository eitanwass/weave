import React from "react";
import {Circle} from "react-konva";
import {distance, normalize, roundTo} from "../../utils/mathUtils";


const gridSpacing = 50;

const proximityRadius = 250;

/**
 * Create circles around point, in radius, with fading opacity level
 * @param mousePos
 */
export const createSurroundingGrid = (mousePos: stagePosType) => {
	const startX = roundTo(mousePos.x - proximityRadius, gridSpacing);
	const endX = roundTo(mousePos.x + proximityRadius + gridSpacing, gridSpacing);

	const startY = roundTo(mousePos.y - proximityRadius, gridSpacing);
	const endY = roundTo(mousePos.y + proximityRadius + gridSpacing, gridSpacing);

	const gridComponents = [];
	for (let x = startX; x < endX; x += gridSpacing) {
		for (let y = startY; y < endY; y += gridSpacing) {

			const d = distance(mousePos, {x, y});
			const dNorm = normalize(d, 0, 255);

			gridComponents.push(
				<Circle
					key={`${x}-${y}`}
					x={x}
					y={y}
					width={3}
					height={3}
					fill={`rgb(255, 255, 255, ${dNorm})`}
				/>
			);
		}
	}
	return gridComponents;
};

export const snapToGrid = (pos: stagePosType) =>
	({x: roundTo(pos.x, gridSpacing), y: roundTo(pos.y, gridSpacing)})

type stagePosType = {
	x: number,
	y: number
}
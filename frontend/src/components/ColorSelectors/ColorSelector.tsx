import {useState} from "react";
import classNames from "classnames";

import "./ColorSelector.css";
import {faPencil} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {useColor} from "../../contexts/ColorContext";

const ColorSelector = ({initialColor = "#ffffff"}: {initialColor?: string}) => {
	const [localColor, setLocalColor] = useState(initialColor);
	const {color, setColor} = useColor();

	return (
		<div
			className={classNames("color-selector", {"selected": color === localColor})}
			style={{
				background: localColor,
			}}
			onClick={() => setColor(localColor)}
		>
			<FontAwesomeIcon className={"edit-icon"} icon={faPencil} />
		</div>
	)

};

export default ColorSelector;

import {ReactNode} from "react";

import "./ColorSelectorsContainer.css";

const ColorSelectorsContainer = ({children}: {children: ReactNode}) => {
	return (
		<div className={"color-selectors-container"}>
			{children}
		</div>
	);
};

export default ColorSelectorsContainer;

import React, {createContext, useContext, useState, ReactNode} from "react";
import {position} from "../types/shapeTypes";


type GlobalMousePositionContextType = {
	globalMousePosition: position,
	setGlobalMousePosition: React.Dispatch<React.SetStateAction<position>>
}

type globalMousePositionProviderType = {
	children: ReactNode,
	state: [position, React.Dispatch<React.SetStateAction<position>>]
}

const GlobalMousePositionContext = createContext<GlobalMousePositionContextType>({
	globalMousePosition: {x: 0, y: 0},
	setGlobalMousePosition: () => {}
});

export const GlobalMousePositionProvider = ({children, state}: globalMousePositionProviderType) => {
	return (
		<GlobalMousePositionContext.Provider
		value={{
			globalMousePosition: state[0],
			setGlobalMousePosition: state[1]
		}}>
			{children}
		</GlobalMousePositionContext.Provider>
	)
};

export const useGlobalMousePosition = () => {
	const currentGlobalMousePositionContext = useContext(GlobalMousePositionContext);

	if(currentGlobalMousePositionContext === undefined) {
		throw new Error(
			"useGlobalMousePosition has to be used within <GlobalMousePositionProvider/>"
		);
	}
	return currentGlobalMousePositionContext;
}
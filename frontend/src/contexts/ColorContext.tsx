import React, {createContext, useContext, useState, ReactNode} from "react";


type ColorContextType = {
	color: string,
	setColor: React.Dispatch<React.SetStateAction<string>>
}

type colorProviderType = {
	children: ReactNode,
	defaultColor: string
}

const ColorContext = createContext<ColorContextType>({
	color: "#000000",
	setColor: () => {}
});

export const ColorProvider = ({children, defaultColor}: colorProviderType) => {
	const [color, setColor] = useState(defaultColor);
	return (
		<ColorContext.Provider
		value={{color, setColor}}>
			{children}
		</ColorContext.Provider>
	)
};

export const useColor = () => {
	const currentColorContext = useContext(ColorContext);

	if(currentColorContext === undefined) {
		throw new Error(
			"useColor has to be used within <ColorProvider/>"
		);
	}
	return currentColorContext;
}
type Point = {
	x: number,
	y: number,
}

export const roundTo = (a: number, rounder: number): number => (
	Math.floor(a / rounder) * rounder
);

export const distance = (p1: Point, p2: Point) => Math.hypot(p2.x - p1.x, p2.y - p1.y)

export const normalize = (val: number, max: number, min: number) => (val - min) / (max - min);

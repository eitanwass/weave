import React, {useState, useRef} from 'react';
import './App.css';
import {GlobalMousePositionProvider} from "./contexts/GlobalMousePositionContext";
import Map from "./components/Map/Map";
import Toolbox from "./components/Toolbox/Toolbox";
import {generalShape} from "./types/shapeTypes";
import FillItem, {FillItemHandle} from "./components/Toolbox/ToolboxItems/FillItem";
import ColorSelector from "./components/ColorSelectors/ColorSelector";
import ColorSelectorsContainer from "./components/ColorSelectors/ColorSelectorsContainer";
import {ColorProvider} from "./contexts/ColorContext";
import Sidebar from "./components/Sidebar/Sidebar";


const App = () => {
  const fillItemRef = useRef<FillItemHandle>(null);

  const [globalMousePosition, setGlobalMousePosition] = useState({x: 0, y: 0});

  const [mapShapes, setMapShapes] = useState<generalShape[]>([]);
  const [currentSelectedShape, setCurrentSelectedShape] = useState<generalShape | undefined>(undefined);

  const addMapShape = (shape: generalShape) => setMapShapes((cur) => [...cur, shape]);

  const onMouseDown = (evt: MouseEvent) => {
    fillItemRef.current?.fillItem.onMouseDown?.(evt, setCurrentSelectedShape, addMapShape);
  };

  const onMouseMove = (evt: MouseEvent) => {
    fillItemRef.current?.fillItem.onMouseMove?.(evt, setCurrentSelectedShape, addMapShape);
  };

  const onMouseUp = (evt: MouseEvent) => {
    fillItemRef.current?.fillItem.onMouseUp?.(evt, setCurrentSelectedShape, addMapShape);
  };

  const onKeyDown = (evt: React.KeyboardEvent<HTMLDivElement>) => {
    fillItemRef.current?.fillItem.onKeyDown?.(evt, setCurrentSelectedShape);
  }

  return (
    <div className="board"
         tabIndex={-1}
         onKeyDown={onKeyDown}
    >
      <GlobalMousePositionProvider state={[globalMousePosition, setGlobalMousePosition]}>
        <Map
          currentEditShape={currentSelectedShape}
          mapShapes={mapShapes}

          onMouseDown={onMouseDown}
          onMouseMove={onMouseMove}
          onMouseUp={onMouseUp}
        />
        <ColorProvider defaultColor={"#969696"}>
          <Sidebar>
            <Toolbox>
              <FillItem ref={fillItemRef}/>
            </Toolbox>
            <ColorSelectorsContainer>
              <ColorSelector />
              <ColorSelector initialColor={"#ff0000"}/>
              <ColorSelector initialColor={"#00ff00"}/>
              <ColorSelector initialColor={"#0000ff"}/>
              <ColorSelector initialColor={"#969696"}/>
              <ColorSelector/>
              <ColorSelector/>
            </ColorSelectorsContainer>
          </Sidebar>
        </ColorProvider>
      </GlobalMousePositionProvider>
    </div>
  );
}

export default App;
